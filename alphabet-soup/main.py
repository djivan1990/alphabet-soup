
def find_word(wordsearch:list,word:str):
    """Tries to find the word in wordsearch and prints results"""

    # store first character positions in array:
    start_pos: list = []
    first_char: str = word[0]

    # loop through wordsearch list:
    for i in range(0, len(wordsearch)):
        for j in range(0, len(wordsearch[i])):
            if wordsearch[i][j] == first_char:
                start_pos.append([i,j])

    # check all starting positions for word:
    for p in start_pos:
        if check_start(wordsearch,word,p):
            #if word found:
            return

    #if word not found:
    print("Word not found.")

def check_start(wordsearch:list, word:str, start_pos:list):
    """Checks if the word is at the start pos. Returns true if word is found"""

    #all possible directions, omit [0,0] because that is not a valid direction:
    directions: list = [[-1,1], [0,1], [1,1], [-1,0], [1,0], [-1,-1], [0,-1], [1,-1]]

    for d in directions:
        if check_dir(wordsearch,word,start_pos,d):
            return  True

def check_dir(wordsearch:list, word:str, start_pos:list, dir:list):
    """Checks if the word is in a direction dir from the start_pos position in the wordsearch. Returns True and prints the result if word is found"""

    # Characters found in direction. Already found the first character:
    found_chars: list = [word[0]]
    # Current position we are looking at:
    current_pos: list = start_pos
    # Keep track of positions we have looked at:
    pos: list = [start_pos]

    #loop over characters in word and wordsearch, see if any match:
    while chars_match(found_chars,word):
        if len(found_chars) == len(word):
            #If found all characters and all characters are correct, then the word has been found:
            print("".join(found_chars) + " " + str(pos[0][0]) + ":" +str(pos[0][1]) + " " + str(current_pos[0]) + ":" + str(current_pos[1]))

            return True

        # Have not found enough letters so look at the next one:
        current_pos = [current_pos[0] + dir[0], current_pos[1] + dir[1]]
        pos.append(current_pos)

        if is_valid_index(wordsearch, current_pos[0], current_pos[1]):
            found_chars.append(wordsearch[current_pos[0]][current_pos[1]])
        else:
            # Reached edge of wordsearch and not found word:
            return

def chars_match(found:list, word:str):
    """Checks if the letters found are the start of the word we are looking for"""

    index: int = 0
    for i in found:
        if i != word[index]:
            return False
        index+=1

    return True

def is_valid_index(wordsearch:list, line_num:int, col_num:int):
    """Checks if the provided line number and column number are valid"""

    if 0 <= line_num < len(wordsearch):
        if 0 <= col_num < len(wordsearch[line_num]):
            return True

    return  False



###########################################################################################
# Program Starts Here
###########################################################################################

# Get Filepath
file:str = input("Enter the full filepath to the wordsearch txt file: ")
wordsearch:list = open(file).read().splitlines()

tempList:list = []

#make sure all whitespace is removed:
for m in wordsearch:
    word = str(m).replace(" ","")
    tempList.append(word)

#set wordsearch list to new list (no whitespace):
wordsearch = tempList

gridSize:str = wordsearch[0]
#remove gridSize from list:
wordsearch.remove(wordsearch[0])

#Items to remove from wordsearch list:
tempList:list = []

#words to search for:
wordsToSearchFor: list = []

#append the words we need to find to the wordsToSearchFor list:
for i in range(len(wordsearch)):
    if i >= int(gridSize[-1]):
        wordsToSearchFor.append(wordsearch[i])
        tempList.append(wordsearch[i])

#loop through words in wordsToSearchFor list:
for i in range(len(wordsToSearchFor)):
    find_word(wordsearch,wordsToSearchFor[i])